# Catalogue Theme

Provides a theme for the catalogue distribution. It is meant to be used as both
the main theme and the administration theme.
